import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import 'rxjs/Rx';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }
  getData() {
    return this.http.get('/assets/metabolites.json')
        .map(
            response => response
        )
        .catch(
            error => error
        );
  }
}
