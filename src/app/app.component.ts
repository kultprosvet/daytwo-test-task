import {AfterViewInit, Component, ElementRef, ViewChild, enableProdMode} from '@angular/core';
import {chart} from 'highcharts';
import * as Highcharts from 'highcharts';
import jsPDF from 'jspdf';
import {ApiService} from './api.service';
import { logo, pageTitle, pageText, pdfFileName, pdfTitle, pdfText, graph1Title, graph2Title, vitamineDescription } from '../assets/assets';

declare var require: any;
require('highcharts/highcharts-more')(Highcharts);
require('highcharts/modules/exporting')(Highcharts);
require('highcharts/modules/offline-exporting')(Highcharts);
import canvg from 'canvg';

enableProdMode();

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements AfterViewInit {
    pageTitle = pageTitle;
    pageText = pageText;
    probioticValue = 6.12;

    @ViewChild('gaugeChartTarget') gaugeChartTarget: ElementRef;
    @ViewChild('chartTarget') chartTarget: ElementRef;

    gaugeChart: Highcharts.ChartObject;
    chart: Highcharts.ChartObject;

    constructor(private apiService: ApiService) {
    }

    ngAfterViewInit() {

        /** GET data from the server */
        this.getGraphData();

        const gaugeOptions: Highcharts.Options = {
            chart: {
                type: 'column',
                inverted: true,
                height: 128
            },
            title: {
                text: 'Probiotic Meter'
            },
            navigation: {
                buttonOptions: {
                    enabled: false
                }
            },
            exporting: {
                fallbackToExportServer: false
            },
            credits: {
                enabled: false
            },
            xAxis: {
                lineColor: 'transparent',
                labels: {
                    enabled: false
                },
                tickLength: 0
            },
            yAxis: {
                min: 0,
                max: 10,
                tickLength: 0,
                tickWidth: 1,
                tickColor: 'transparent',
                gridLineColor: 'transparent',
                gridLineWidth: 0,
                minorTickInterval: 5,
                minorTickWidth: 0,
                minorTickLength: 5,
                minorGridLineWidth: 0,
                reversed: true,
                title: null,
                labels: {
                    enabled: false
                },
                plotBands: [{
                    from: 9,
                    to: 10,
                    color: '#63bf7b'
                }, {
                    from: 8,
                    to: 9,
                    color: '#77d077'
                }, {
                    from: 7,
                    to: 8,
                    color: '#a0d977'
                }, {
                    from: 6,
                    to: 7,
                    color: '#c5e278'
                }, {
                    from: 5,
                    to: 6,
                    color: '#ebea77'
                }, {
                    from: 4,
                    to: 5,
                    color: '#fade76'
                }, {
                    from: 3,
                    to: 4,
                    color: '#f9be73'
                }, {
                    from: 2,
                    to: 3,
                    color: '#f89d70'
                }, {
                    from: 1,
                    to: 2,
                    color: '#f77a6c'
                }, {
                    from: 0,
                    to: 1,
                    color: '#f65569'
                }]
            },
            legend: {
                enabled: false
            },
            tooltip: {
                enabled: false
            },
            plotOptions: {
                column: {
                    dataLabels: {
                        enabled: true,
                        align: 'center',
                        formatter: function () {
                            const className = this.y === 0 || this.y === 10 ? 'axis-label' : 'pointer';
                            return '<div class="' + className + '">' + this.y + '</div>';
                        },
                        useHTML: true
                    },
                    maxPointWidth: 50,
                    // showInLegend: false,
                    borderWidth: 0,
                    color: 'transparent'

                }
            },
            series: [
                { data: [0, 10] },
                { data: [this.probioticValue] },
            ]
        };
        this.gaugeChart = chart(this.gaugeChartTarget.nativeElement, gaugeOptions);

        const options: Highcharts.Options = {
            chart: {
                type: 'columnrange',
            },
            title: {
                text: ' '
            },
            navigation: {
                buttonOptions: {
                    enabled: false
                }
            },
            exporting: {
                fallbackToExportServer: false
            },
            credits: {
                enabled: false
            },
            yAxis: {
                title: {
                    text: ''
                },
                labels: {
                    formatter: function () {
                        return this.value + '%';
                    },
                },
                max: 100,
                min: 0,
                plotLines: [{
                    value: 50,
                    color: 'black',
                    dashStyle: 'solid',
                    width: 2,
                    label: {
                        text: ' '
                    }
                }],
            },
            xAxis: {
                type: 'category',
                labels: {
                    formatter: function () {
                        return '<i>' + this.value + '</i>';
                    },
                }
            },
            plotOptions: {
                columnrange: {
                    dataLabels: {
                        enabled: false,
                        format: '{y}%'
                    },
                    colorByPoint: true,
                    maxPointWidth: 50,
                    // showInLegend: false,
                    borderColor: 'black',
                    borderWidth: 2,

                }
            },
            tooltip: {
                useHTML: true,
                formatter: function () {
                    return '<div style="width: 400px; white-space:normal;">' + this.point.description + '</div>';
                }
            },
        };
        this.chart = chart(this.chartTarget.nativeElement, options);
    }

    downloadPDF() {
        const doc = new jsPDF();
        doc.addImage(logo, 'PNG', 170, 10, 26, 11);
        doc.setFontSize(16);
        doc.setFontType('bold');
        doc.text(20, 35, pdfTitle);

        doc.setFontSize(14);
        doc.setFontType('normal');
        const splitText = doc.splitTextToSize(pdfText, 170);
        doc.text(20, 45, splitText);

        // Draw rectangle
        doc.setDrawColor(215, 215, 215);
        doc.roundedRect(10, 100, 190, 130, 2, 2);
        // Render graph titles
        doc.setFontType('bold');
        doc.text(20, 115, graph1Title);
        doc.setFontType('normal');
        doc.setTextColor(158, 158, 158);
        doc.setFontSize(12);
        doc.text(25, 190, graph2Title, null, 90);


        // Draw the Probiotic Meter
        doc.setDrawColor(246, 85, 105);
        doc.setFillColor(246, 85, 105);
        doc.roundedRect(90, 110, 11, 10, 1, 1, 'FD');

        doc.setDrawColor(247, 122, 108);
        doc.setFillColor(247, 122, 108);
        doc.rect(100, 110, 10, 10, 'FD');

        doc.setDrawColor(248, 157, 112);
        doc.setFillColor(248, 157, 112);
        doc.rect(110, 110, 10, 10, 'FD');

        doc.setDrawColor(249, 190, 115);
        doc.setFillColor(249, 190, 115);
        doc.rect(120, 110, 10, 10, 'FD');

        doc.setDrawColor(250, 222, 118);
        doc.setFillColor(250, 222, 118);
        doc.rect(130, 110, 10, 10, 'FD');

        doc.setDrawColor(235, 234, 119);
        doc.setFillColor(235, 234, 119);
        doc.rect(140, 110, 10, 10, 'FD');

        doc.setDrawColor(197, 226, 120);
        doc.setFillColor(197, 226, 120);
        doc.rect(150, 110, 10, 10, 'FD');

        doc.setDrawColor(160, 217, 119);
        doc.setFillColor(160, 217, 119);
        doc.rect(160, 110, 10, 10, 'FD');

        doc.setDrawColor(99, 191, 123);
        doc.setFillColor(99, 191, 123);
        doc.roundedRect(179, 110, 11, 10, 1, 1, 'FD');

        doc.setDrawColor(119, 208, 119);
        doc.setFillColor(119, 208, 119);
        doc.rect(170, 110, 10, 10, 'FD');

        doc.setFontType('bold');
        doc.setTextColor(255, 255, 255);
        doc.setFontSize(14);
        doc.text(94, 117, '1');
        doc.text(182, 117, '10');

        // Set the probiotic pointer
        doc.setDrawColor(94, 160, 224);
        doc.setFillColor(94, 160, 224);
        doc.roundedRect(85 + this.probioticValue * 9, 109, 15, 12, 6, 6, 'FD');
        doc.text(88 + this.probioticValue * 9, 117, String(this.probioticValue));

        // Render Vitamin chart
        const canvas = document.createElement('canvas');
        const context = canvas.getContext('2d');
        const chartSvg = this.chart.getSVG();
        context.clearRect(0, 0, canvas.width, canvas.height);
        canvg(canvas, chartSvg);
        const chartImgData = canvas.toDataURL('image/png');
        doc.addImage(chartImgData, 'JPEG', 35, 130, 160, 100);

        doc.save(pdfFileName);
    }

    getGraphData() {
        this.apiService.getData()
            .subscribe(
                (data: object) => {
                    for (const nutrientName of Object.keys(data['metabolites'].essential_nutrients)) {
                        const graphData = [];
                        const nutrient = data['metabolites'].essential_nutrients[nutrientName];
                        for (const vitaminName of Object.keys(nutrient)) {
                            const vitamin = nutrient[vitaminName];
                            graphData.push({
                                low: 50,
                                high: Number(vitamin.personal.percentile),
                                name: vitamineDescription[vitaminName] ? vitamineDescription[vitaminName].name : vitaminName,
                                description: vitamineDescription[vitaminName] ? vitamineDescription[vitaminName].description : 'No description',
                                color: Number(vitamin.personal.percentile) >= 50 ? '#6fad47' : '#bf281b'
                            });
                        }
                        this.chart.addSeries({
                            name: nutrientName,
                            data: graphData
                        });
                    }
                },
                error => console.log(error)
            );
    }

}
